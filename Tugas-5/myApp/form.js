import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                <TextInput style={styles.inputBox}
                 placeholder='Username/email' 
                 />
                 <TextInput style={styles.inputBox2}
                 placeholder='Password'
                 secureTextEntry={true}
                 />
                <View style = {styles.forgotTextCont}>
                <Text style={styles.forgotButton}>Forgot Password?</Text> 
                </View>
                 <TouchableOpacity style={styles.button}>
                     <Text style={styles.buttonText}>Login</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center'
        },
    inputBox:{
        width:300,
        height:50,
        backgroundColor: '#FFEFD5',
        borderRadius: 15,
        paddingHorizontal:10,
        fontSize: 16,
        marginTop: "65%",
    },
    inputBox2:{
        width:300,
        height:50,
        backgroundColor: '#FFEFD5',
        borderRadius: 15,
        paddingHorizontal:10,
        fontSize: 16,
        marginTop: 15,
    },

    forgotButton: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500',
        marginTop:15,
        alignItems:'flex-end',
    },
    button:{
        width: '40%',
        height:40,
        backgroundColor: '#FFDAB9',
        borderRadius:5,
        marginTop:"40%",
        paddingVertical:10,
        alignItems:'center',
    },
    buttonText: {
        fontSize:16,
        color:'#000',
        alignItems:'center',
        marginBottom:20,
    },
});
