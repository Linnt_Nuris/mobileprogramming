import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button, ImageBackground } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <ImageBackground source={require('./assets/biodata.jpg')} style={styles.bgcont}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/lintang.jpg')} />
                    </View>
                    <Text style={styles.name}>Lintang Nuril Islami</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Program study : Teknik Informatika </Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>kelas : Pagi A </Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>TTL : Brebes,30 Desember 1999</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Alamat : Purwakarta</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Instagram : @linnt.nuris</Text>
                </View>
                </ImageBackground>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#2F4F4F',
        flex:1,
        width:'100%',
        height:'100%',
    },

    bgcont:{
        flex:1,
        resizeMode:'cover',
        justifyContent:'center',
        width:'100%',
        height:'100%',
        alignItems:'center'
    },

    bio:{
        textAlign:"center",
    marginTop:5,
    marginBottom:10,
    fontSize:18,
    fontFamily:"serif",
    color:'#000',
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68,
    },
    name: {
        color: '#000',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily:'serif',
        marginBottom:'8%',
    },
});