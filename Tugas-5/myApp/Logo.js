import React, {Component} from 'react';
import { StyleSheet, ImageBackground, Text, View, Image } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style={styles.container}>

            <Image style={styles.image} source={require("./assets/affection.png")}/>
                    <Text style={styles.logoText}>INEFABBLE </Text>
                    <Text style={styles.loginText}>Login</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }, 
    logoText: {
        fontSize: 30,
        color: '#000',
        fontFamily:'serif',
        alignItems:'center',
        textAlign:'center',
        marginBottom:20,
    },
    loginText: {
        fontSize: 20,
        color: '#000',
        fontFamily:'serif',
        alignItems:'center',
        textAlign:'center',
    },
    image: {
        width:90,
        height:90,
        marginTop:"80%",
      },
});
